import tkinter, threading, time

class GameOfLife(tkinter.Tk):
    def __init__(self, width, heigth, interval=1):
        tkinter.Tk.__init__(self)
        self.game = Playground(self, width, heigth)
        self.game.grid(row=0, column=0)
        menu = tkinter.Frame(self)
        tkinter.Button(menu, text="Update", command=self.game.cycle_of_life).pack()
        self.cycling_button = tkinter.Button(menu, text="Cycle\nUpdate", command=self.change_flag, background='red')
        self.cycling_button.pack()
        menu.grid(row=0, column=1)
        self.stop_event = threading.Event()
        self.thread = threading.Thread(target=self.updater, args=(self.stop_event, interval))
        self.thread.start()

    def updater(self, stop_event, interval):
        while True:
            while self.stop_event.is_set():
                self.game.cycle_of_life()
                time.sleep(1)
            time.sleep(1)

    def change_flag(self):
        if self.stop_event.is_set():
            self.stop_event.clear()
            self.cycling_button.config(background='red')
        else:
            self.stop_event.set()
            self.cycling_button.config(background='green')
class Cell(tkinter.Button):

    def __init__(self, master=None, row=None, column=None):
        self.color = 'white'
        tkinter.Button.__init__(self, master=master, command=self.update, background=self.color, activebackground=self.color)
        self.row = row
        self.column = column
        if row == None or column == None:
            self.pack()
        else:
            self.grid(row=row, column=column)

    def update(self):
        if self.color == 'white':
            self.color = 'red'
            self.master.matrix[self.row][self.column] = 1
        else:
            self.color = 'white'
            self.master.matrix[self.row][self.column] = 0
        self.config(background=self.color, activebackground=self.color)

    def check_state(self):
        if self.color == 'red' and self.master.matrix[self.row][self.column] == 0:
            self.color = 'white'
            self.config(background=self.color, activebackground=self.color)
        elif self.color == 'white' and self.master.matrix[self.row][self.column] == 1:
            self.color = 'red'
            self.config(background=self.color, activebackground=self.color)



class Playground(tkinter.Frame):
    def __init__(self, master, width, heigth, rules_of_creation=[3], rules_of_surviwal=[2,3]):
        tkinter.Frame.__init__(self, master=master)
        self.rules_of_creation = rules_of_creation
        self.rules_of_surviwal = rules_of_surviwal
        self.matrix = [[0]*width for i in range(0,heigth)]
        self.width = width
        self.height = heigth
        for row in range(0, heigth):
            for column in range(0, width):
                Cell(self, row, column)
        # print (self.grid_slaves())
    def cycle_of_life(self):

        def append_support_matrix(self, support_matrix,i,j):
            for row in [-1,0,1]:
                for column in [-1,0,1]:
                    if ((i+row) in range(0,self.height-1)) and ((j+column) in range(0, self.width-1)):
                        support_matrix[i+row][j+column]+=1

        support_matrix = [[0]*self.width for i in range(0, self.height)]
        for i in range(0, self.height):
            for j in range(0, self.width):
                if self.matrix[i][j] == 1:
                    append_support_matrix(self, support_matrix, i, j)
        for i in range(0, self.height):
            for j in range(0, self.width):
                if support_matrix[i][j] in self.rules_of_creation and self.matrix[i][j] == 0:
                    self.matrix[i][j] = 1
                elif support_matrix[i][j]-1 in self.rules_of_surviwal and self.matrix[i][j] == 1:
                    self.matrix[i][j] = 1
                else:
                    self.matrix[i][j] = 0
        # print (self.matrix)
        for objects in self.grid_slaves():
            objects.check_state()

if __name__ == '__main__':
    GameOfLife(50, 50)
    tkinter.mainloop()
